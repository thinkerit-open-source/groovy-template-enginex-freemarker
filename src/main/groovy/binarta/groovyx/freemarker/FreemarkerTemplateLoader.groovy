package binarta.groovyx.freemarker

import freemarker.cache.TemplateLoader
import groovy.transform.CompileStatic

@CompileStatic
class FreemarkerTemplateLoader implements TemplateLoader {
    @Override
    Object findTemplateSource(String name) throws IOException {
        FreemarkerTemplateEngine.reader
    }

    @Override
    long getLastModified(Object templateSource) {
        return 0
    }

    @Override
    Reader getReader(Object templateSource, String encoding) throws IOException {
        (Reader) templateSource
    }

    @Override
    void closeTemplateSource(Object templateSource) throws IOException {
    }
}
