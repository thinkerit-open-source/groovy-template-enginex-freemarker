package binarta.groovyx.freemarker

import freemarker.core.Environment
import freemarker.template.TemplateDirectiveBody
import freemarker.template.TemplateDirectiveModel
import freemarker.template.TemplateException
import freemarker.template.TemplateModel
import groovy.transform.CompileStatic
import org.junit.Test

import static java.util.Collections.emptyMap

class FreemarkerTemplateEngineSpec {
    private FreemarkerTemplateEngine engine = new FreemarkerTemplateEngine()

    @Test void "rendering a template without markup returns the unmodified input"() {
        assert render('no-markup') == 'no-markup'
    }

    @Test void "rendering with string interpolation"() {
        assert render('Hello ${who}!', who: 'World') == 'Hello World!'
    }

    @Test void "rendering with custom tag"() {
        engine.put('hello', new HelloDirective())
        assert render('<@hello who="World"></@hello>') == 'Hello World!'
    }

    @Test void "rendering with custom tag and string interpolation"() {
        engine.put('hello', new HelloDirective())
        assert render('<@hello who="${who}"></@hello>', who: 'World') == 'Hello World!'
    }

    private String render(String templateString) {
        render(emptyMap(), templateString)
    }

    private String render(Map binding, String templateString) {
        StringWriter out = new StringWriter()
        engine.createTemplate(new StringReader(templateString)).make(binding).writeTo(out)
        return out.toString()
    }

    @CompileStatic
    private static class HelloDirective implements TemplateDirectiveModel {
        @Override
        void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
            env.out << "Hello $params.who!"
        }
    }
}